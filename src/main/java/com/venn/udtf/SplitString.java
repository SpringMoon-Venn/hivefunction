package com.venn.udtf;

import org.apache.commons.lang.StringUtils;
import org.apache.hadoop.hive.ql.exec.UDFArgumentException;
import org.apache.hadoop.hive.ql.exec.UDFArgumentLengthException;
import org.apache.hadoop.hive.ql.metadata.HiveException;
import org.apache.hadoop.hive.ql.udf.generic.GenericUDTF;
import org.apache.hadoop.hive.serde2.objectinspector.ObjectInspector;
import org.apache.hadoop.hive.serde2.objectinspector.ObjectInspectorFactory;
import org.apache.hadoop.hive.serde2.objectinspector.StructObjectInspector;
import org.apache.hadoop.hive.serde2.objectinspector.primitive.PrimitiveObjectInspectorFactory;

import java.util.ArrayList;

/**
 * Created by venn on 5/20/2018.
 * SplitString : split string
 * first 3 string : country
 * next 3 string : province
 * next 3 string : city
 * next all : story
 */
public class SplitString extends GenericUDTF {

    /**
     * add the column name
     * @param args
     * @return
     * @throws UDFArgumentException
     */
    @Override
    public StructObjectInspector initialize(ObjectInspector[] args) throws UDFArgumentException {
        if (args.length != 1) {
            throw new UDFArgumentLengthException("ExplodeMap takes only one argument");
        }
        if (args[0].getCategory() != ObjectInspector.Category.PRIMITIVE) {
            throw new UDFArgumentException("ExplodeMap takes string as a parameter");
        }

        ArrayList<String> fieldNames = new ArrayList<String>();
        ArrayList<ObjectInspector> fieldOIs = new ArrayList<ObjectInspector>();
        fieldNames.add("userid");
        fieldOIs.add(PrimitiveObjectInspectorFactory.javaStringObjectInspector);
        fieldNames.add("country");
        fieldOIs.add(PrimitiveObjectInspectorFactory.javaStringObjectInspector);
        fieldNames.add("province");
        fieldOIs.add(PrimitiveObjectInspectorFactory.javaStringObjectInspector);
        fieldNames.add("city");
        fieldOIs.add(PrimitiveObjectInspectorFactory.javaStringObjectInspector);
        fieldNames.add("story");
        fieldOIs.add(PrimitiveObjectInspectorFactory.javaStringObjectInspector);

        return ObjectInspectorFactory.getStandardStructObjectInspector(fieldNames, fieldOIs);
    }

    /**
     * process the column
     * @param objects
     * @throws HiveException
     */
    public void process(Object[] objects) throws HiveException {

        String[] result = new String[5];
        try {
            /*System.out.println(objects[0].toString());
            System.out.println(objects[0] != null);
            System.out.println(StringUtils.isEmpty(objects[0].toString()));
            System.out.println(objects[0].toString().length() < 10);*/
            if (objects[0] == null || StringUtils.isEmpty(objects[0].toString()) || objects[0].toString().length() < 10) {

                result[0] = "0";
                result[1] = "0";
                result[2] = "0";
                result[3] = "0";
                result[4] = "0";
            } else {
                result[0] = objects[0].toString();
                result[1] = objects[0].toString().substring(0, 3);
                result[2] = objects[0].toString().substring(3, 6);
                result[3] = objects[0].toString().substring(6, 9);
                result[4] = objects[0].toString().substring(9);

            }

            forward(result);

        } catch (Exception e) {

        }

    }

    public void close() throws HiveException {

    }
}
