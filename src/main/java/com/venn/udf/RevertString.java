package com.venn.udf;

import org.apache.hadoop.hive.ql.exec.UDF;

/**
 * Created by venn on 5/20/2018.
 *
 * revert string
 */
public class RevertString extends UDF {

    /**
     * evalute function: use overload 可以重载，自动识别
     * @param str
     * @return
     */
    public String evaluate(String str) {

        // if string is null or ""
        if(org.apache.commons.lang.StringUtils.isEmpty(str.trim())){
            return "";
        }
        int len = str.length();
        char[] chars = new char[len];
        // revert string
        for(int i=0; i < len; i++){
            chars[i] = str.charAt(len - i -1);
        }

        return new String(chars).toString();
    }

    public static void main(String[] args) {
        String userid = "1234567890";
        String str = null;
        System.out.println(str.toString());
        System.out.println(userid.charAt(0));
        System.out.println(new RevertString().evaluate(userid));
    }

}
