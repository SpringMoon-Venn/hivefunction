# hivefunction

#### 项目介绍
hive function demo

#### 软件架构
软件架构说明


#### 安装教程

1. git clone source code
2. mvn package the hivefunction_xx.jar
3. upload to server
4. modify the $HIVE_HOME/bin/.hiverc like :
~~~
add jar /opt/hadoop/idp_hd/viewstat/lib/hivefunction-1.0-SNAPSHOT.jar;
create temporary function revert_string as 'com.venn.udf.RevertString';
create temporary function split_area as 'com.venn.udtf.SplitString';
~~~
5. restart hive client, and use the function like :
~~~
hive>
    > select revert_string(userid),userid from sqoop_test limit 10;
OK
18185403678	87630458181
77375403678	87630457377
77375403678	87630457377
27175403678	87630457172
75175403678	87630457157
20365403678	87630456302
25555403678	87630455552
25555403678	87630455552
34555403678	87630455543
17545403678	87630454571
~~~
~~~
hive> select split_area(userid) from sqoop_test limit 10;
OK
87630458181	876	304	581	81
87630457377	876	304	573	77
87630457377	876	304	573	77
87630457172	876	304	571	72
87630457157	876	304	571	57
87630456302	876	304	563	02
87630455552	876	304	555	52
87630455552	876	304	555	52
87630455543	876	304	555	43
87630454571	876	304	545	71
~~~

#### 使用说明

1. udf : 一行输入，一行输出
2. udtf : 一行输入，多行输出
3. udaf : 多行输入，一行输出，一般在group by中使用group by

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [http://git.mydoc.io/](http://git.mydoc.io/)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)