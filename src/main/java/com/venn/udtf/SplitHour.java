package com.venn.udtf;

import org.apache.hadoop.hive.ql.exec.UDFArgumentException;
import org.apache.hadoop.hive.ql.exec.UDFArgumentLengthException;
import org.apache.hadoop.hive.ql.metadata.HiveException;
import org.apache.hadoop.hive.ql.udf.generic.GenericUDTF;
import org.apache.hadoop.hive.serde2.objectinspector.ObjectInspector;
import org.apache.hadoop.hive.serde2.objectinspector.ObjectInspectorFactory;
import org.apache.hadoop.hive.serde2.objectinspector.StructObjectInspector;
import org.apache.hadoop.hive.serde2.objectinspector.primitive.PrimitiveObjectInspectorFactory;

import java.util.ArrayList;

/**
 * Created by venn on 5/20/2018.
 * SplitString : split string
 * first 3 string : country
 * next 3 string : province
 * next 3 string : city
 * next all : story
 */
public class SplitHour extends GenericUDTF {

    /**
     * add the column name
     * @param args
     * @return
     * @throws UDFArgumentException
     */
    @Override
    public StructObjectInspector initialize(ObjectInspector[] args) throws UDFArgumentException {
        if (args.length != 1) {
            throw new UDFArgumentLengthException("ExplodeMap takes only one argument");
        }
        if (args[0].getCategory() != ObjectInspector.Category.PRIMITIVE) {
            throw new UDFArgumentException("ExplodeMap takes string as a parameter");
        }

        ArrayList<String> fieldNames = new ArrayList<String>();
        ArrayList<ObjectInspector> fieldOIs = new ArrayList<ObjectInspector>();
        fieldNames.add("begintime");
        fieldOIs.add(PrimitiveObjectInspectorFactory.javaStringObjectInspector);
        fieldNames.add("endtime");
        fieldOIs.add(PrimitiveObjectInspectorFactory.javaStringObjectInspector);
        fieldNames.add("hour");
        fieldOIs.add(PrimitiveObjectInspectorFactory.javaStringObjectInspector);
        fieldNames.add("seconds");
        fieldOIs.add(PrimitiveObjectInspectorFactory.javaStringObjectInspector);


        return ObjectInspectorFactory.getStandardStructObjectInspector(fieldNames, fieldOIs);
    }

    /**
     * process the column
     * @param objects
     * @throws HiveException
     */
    public void process(Object[] objects) throws HiveException {


        String [] input = objects[0].toString().split(",");
        // 2018-06-06 10:25:35
        String beginTime = input[0];
        String endTime = input[1];

        String[] result = new String[4];
        result[0] = beginTime;
        result[1] = endTime;

        // begintime
        int bhour = Integer.parseInt(beginTime.substring(11, 13));
        int bmin = Integer.parseInt(beginTime.substring(14, 16));
        int bsecond = Integer.parseInt(beginTime.substring(17, 19));
        // endtime
        int ehour = Integer.parseInt(endTime.substring(11, 13));
        int emin = Integer.parseInt(endTime.substring(14, 16));
        int esecond = Integer.parseInt(endTime.substring(17, 19));

        while (bhour != ehour){
            result[2] = String.valueOf(bhour);
            result[3] = String.valueOf(3600);
            bhour += 1;
            // 输出到hive
            forward(result);
        }

        result[2] = String.valueOf(bhour);
        result[3] = String.valueOf( (emin - bmin) * 60 + (esecond - bsecond) );
        forward(result);

    }

    public void close() throws HiveException {

    }

}
